# Satispay App Extension

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

- iOS ≥ 8.0
- [Satispay](https://itunes.apple.com/it/app/satispay/id790287076?mt=8) ≥ 2.0.22

## Installation (using CocoaPods)

The *Satispay App Extension* support framework is available through [CocoaPods](http://cocoapods.org).

To install
it, simply add the following lines to your Podfile:

```ruby
source "https://bitbucket.org/satispay/ios_pods.git"
pod "SatispayAppExtension"
```

To use the extension in a Swift project, also add the following line to your Podfile:

```ruby
use_frameworks!
```

and then import `SatispayAppExtension` in your Swift/Obj-C files:

```swift
import SatispayAppExtension
```

```objectivec
@import SatispayAppExtension;
```

## Installation (copying source files)

The support framework can also be used if the integrating project doesn't use CocoaPods.
To do so, just drag the source files found in the `SatispayAppExtension/Classes/` directory into the target project.

## Usage

Detailed usage instructions are available as comments into the `SatispayAppExtension/Classes/SatispayExtension.h` header file and implemented in the `Example` project.
